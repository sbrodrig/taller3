#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>


void reemplazar(char linea[], char viejo, char nuevo);

int main (){
	char numArray[LINE_MAX]={0};
	int acumulador=0;
    float num;
	float prom;
	float sum=0;

	printf("Este programa calcula el promedio de numeros\n");
    printf("Use -1 para indicar el final de la lista\n");
    do{
        printf("? ");
		fgets(numArray, LINE_MAX, stdin);
		reemplazar(numArray,'\n','\0');
		num=atof(numArray);
        if(num!=-1){
            sum=sum+num;
            acumulador++;
        }   
    }while(num!=-1);
	
    if(acumulador==0){
		prom=0;
	}
	    
    else{
        prom=sum/(float)acumulador;
	}

	printf("El promedio es %.2f\n",prom);

	return 0;

}

void reemplazar(char linea[], char viejo, char nuevo){
	for(int i=0; i<LINE_MAX; i++){
		if(linea[i]==viejo){
			linea[i]=nuevo;
		}
	}
		
}



